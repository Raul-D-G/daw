﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LKW_Bursa_Transport.Migrations
{
    public partial class CamioaneTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Camioane",
                columns: table => new
                {
                    CamionID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NrInmatriculare = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Marca = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Consum = table.Column<int>(type: "int", nullable: false),
                    Disponibil = table.Column<bool>(type: "bit", nullable: false),
                    CompanieID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Camioane", x => x.CamionID);
                    table.ForeignKey(
                        name: "FK_Camioane_AspNetUsers_CompanieID",
                        column: x => x.CompanieID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Camioane_CompanieID",
                table: "Camioane",
                column: "CompanieID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Camioane");
        }
    }
}
