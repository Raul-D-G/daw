﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LKW_Bursa_Transport.Migrations
{
    public partial class TabelDePiese : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Piese",
                columns: table => new
                {
                    PiesaID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Denumire = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Pret = table.Column<int>(type: "int", nullable: false),
                    NrPiesa = table.Column<int>(type: "int", nullable: false),
                    CompanieID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Piese", x => x.PiesaID);
                    table.ForeignKey(
                        name: "FK_Piese_AspNetUsers_CompanieID",
                        column: x => x.CompanieID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Piese_CompanieID",
                table: "Piese",
                column: "CompanieID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Piese");
        }
    }
}
