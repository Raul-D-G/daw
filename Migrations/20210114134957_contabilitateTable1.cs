﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LKW_Bursa_Transport.Migrations
{
    public partial class contabilitateTable1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contabilitate_AspNetUsers_CompanieID",
                table: "Contabilitate");

            migrationBuilder.DropIndex(
                name: "IX_Contabilitate_CompanieID",
                table: "Contabilitate");

            migrationBuilder.AlterColumn<string>(
                name: "CompanieID",
                table: "Contabilitate",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contabilitate_CompanieID",
                table: "Contabilitate",
                column: "CompanieID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Contabilitate_AspNetUsers_CompanieID",
                table: "Contabilitate",
                column: "CompanieID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contabilitate_AspNetUsers_CompanieID",
                table: "Contabilitate");

            migrationBuilder.DropIndex(
                name: "IX_Contabilitate_CompanieID",
                table: "Contabilitate");

            migrationBuilder.AlterColumn<string>(
                name: "CompanieID",
                table: "Contabilitate",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.CreateIndex(
                name: "IX_Contabilitate_CompanieID",
                table: "Contabilitate",
                column: "CompanieID",
                unique: true,
                filter: "[CompanieID] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Contabilitate_AspNetUsers_CompanieID",
                table: "Contabilitate",
                column: "CompanieID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
