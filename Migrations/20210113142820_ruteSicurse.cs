﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LKW_Bursa_Transport.Migrations
{
    public partial class ruteSicurse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Rute",
                columns: table => new
                {
                    RutaID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TaraIncarcare = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    TaraDescarcare = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rute", x => x.RutaID);
                });

            migrationBuilder.CreateTable(
                name: "Curse",
                columns: table => new
                {
                    CursaID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrasIncarcare = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrasDescarcare = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Km = table.Column<int>(type: "int", nullable: false),
                    Pret = table.Column<int>(type: "int", nullable: false),
                    RutaID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Curse", x => x.CursaID);
                    table.ForeignKey(
                        name: "FK_Curse_Rute_RutaID",
                        column: x => x.RutaID,
                        principalTable: "Rute",
                        principalColumn: "RutaID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Curse_RutaID",
                table: "Curse",
                column: "RutaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Curse");

            migrationBuilder.DropTable(
                name: "Rute");
        }
    }
}
