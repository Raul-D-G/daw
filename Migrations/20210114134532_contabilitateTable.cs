﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LKW_Bursa_Transport.Migrations
{
    public partial class contabilitateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContabilitateID",
                table: "Curse",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Contabilitate",
                columns: table => new
                {
                    ContabilitateID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Venit = table.Column<int>(type: "int", nullable: false),
                    Cheltuieli = table.Column<int>(type: "int", nullable: false),
                    CompanieID = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contabilitate", x => x.ContabilitateID);
                    table.ForeignKey(
                        name: "FK_Contabilitate_AspNetUsers_CompanieID",
                        column: x => x.CompanieID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Curse_ContabilitateID",
                table: "Curse",
                column: "ContabilitateID");

            migrationBuilder.CreateIndex(
                name: "IX_Contabilitate_CompanieID",
                table: "Contabilitate",
                column: "CompanieID",
                unique: true,
                filter: "[CompanieID] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Curse_Contabilitate_ContabilitateID",
                table: "Curse",
                column: "ContabilitateID",
                principalTable: "Contabilitate",
                principalColumn: "ContabilitateID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Curse_Contabilitate_ContabilitateID",
                table: "Curse");

            migrationBuilder.DropTable(
                name: "Contabilitate");

            migrationBuilder.DropIndex(
                name: "IX_Curse_ContabilitateID",
                table: "Curse");

            migrationBuilder.DropColumn(
                name: "ContabilitateID",
                table: "Curse");
        }
    }
}
