﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LKW_Bursa_Transport.Migrations
{
    public partial class tabelAngajat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Angajati",
                columns: table => new
                {
                    AngajatID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Functie = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Nume = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Vechime = table.Column<int>(type: "int", nullable: false),
                    Salariu = table.Column<float>(type: "real", nullable: false),
                    Disponibil = table.Column<bool>(type: "bit", nullable: false),
                    CompanieID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Angajati", x => x.AngajatID);
                    table.ForeignKey(
                        name: "FK_Angajati_AspNetUsers_CompanieID",
                        column: x => x.CompanieID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Angajati_CompanieID",
                table: "Angajati",
                column: "CompanieID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Angajati");
        }
    }
}
