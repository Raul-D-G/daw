﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LKW_Bursa_Transport.Areas.Identity.Data;
using LKW_Bursa_Transport.Models;
using Microsoft.AspNetCore.Identity;

namespace LKW_Bursa_Transport.Controllers
{
    public class PieseController : Controller
    {
        private readonly AuthDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;

        public PieseController(AuthDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Piese
        public async Task<IActionResult> Index(string searchString)
        {
            var user = _userManager.GetUserAsync(User).Result;

            var piese = from p in _context.Piese
                           .Include(v => v.Companie)
                           .Where(c => c.CompanieID == user.Id)
                           select p;

            ViewData["CurrentFilter"] = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                piese = piese.Where(s => s.Denumire.Contains(searchString));
            }


            return View(await piese.AsNoTracking().ToListAsync());
        }

        // GET: Piese/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var piesa = await _context.Piese
                .Include(p => p.Companie)
                .FirstOrDefaultAsync(m => m.PiesaID == id);
            if (piesa == null)
            {
                return NotFound();
            }

            return View(piesa);
        }

        // GET: Piese/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Piese/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Denumire,Pret,NrPiesa")] Piesa piesa)
        {
            try
            {
                var user = _userManager.GetUserAsync(User).Result;
                piesa.CompanieID = user.Id;
                if (ModelState.IsValid)
                {
                    _context.Add(piesa);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

            }
            catch (DbUpdateException)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(piesa);
        }

        // GET: Piese/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var piesa = await _context.Piese.FindAsync(id);
            if (piesa == null)
            {
                return NotFound();
            }
            return View(piesa);
        }

        // POST: Piese/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PiesaID,Denumire,Pret,NrPiesa")] Piesa piesa)
        {
            var user = _userManager.GetUserAsync(User).Result;
            piesa.CompanieID = user.Id;
            if (id != piesa.PiesaID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(piesa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PiesaExists(piesa.PiesaID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(piesa);
        }

        // GET: Piese/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var piesa = await _context.Piese
                .Include(p => p.Companie)
                .FirstOrDefaultAsync(m => m.PiesaID == id);
            if (piesa == null)
            {
                return NotFound();
            }

            return View(piesa);
        }

        // POST: Piese/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var piesa = await _context.Piese.FindAsync(id);
            _context.Piese.Remove(piesa);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PiesaExists(int id)
        {
            return _context.Piese.Any(e => e.PiesaID == id);
        }
    }
}
