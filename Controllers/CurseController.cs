﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LKW_Bursa_Transport.Areas.Identity.Data;
using LKW_Bursa_Transport.Models;

namespace LKW_Bursa_Transport.Controllers
{
    public class CurseController : Controller
    {
        private readonly AuthDbContext _context;

        public CurseController(AuthDbContext context)
        {
            _context = context;
        }

        // GET: Curse
        public async Task<IActionResult> Index(string sortOrder)
        {
            ViewData["KmSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["PretSortParm"] = sortOrder == "Pret" ? "Pret_desc" : "Pret";

            var curse = from s in _context.Curse
                        select s;
            switch (sortOrder)
            {
                case "name_desc":
                    curse = curse.OrderByDescending(s => s.Km);
                    break;
                case "Pret":
                    curse = curse.OrderBy(s => s.Pret);
                    break;
                case "Pret_desc":
                    curse = curse.OrderByDescending(s => s.Pret);
                    break;
                default:
                    curse = curse.OrderBy(s => s.Km);
                    break;
            }
            return View(await curse.AsNoTracking().ToListAsync());

        }

        // GET: Curse/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cursa = await _context.Curse
                .Include(c => c.Ruta)
                .FirstOrDefaultAsync(m => m.CursaID == id);
            if (cursa == null)
            {
                return NotFound();
            }

            return View(cursa);
        }

        // GET: Curse/Create
        public IActionResult CreateAsync()
        {
            ViewData["RutaID"] = new SelectList(_context.Rute, "RutaID", "RutaID");
            ViewData["TaraIncarcare"] = new SelectList(_context.Rute, "TaraIncarcare", "TaraIncarcare").GroupBy(x => x.Text).Select(x => x.First()).ToList();
            ViewData["TaraDescarcare"] = new SelectList(_context.Rute, "TaraDescarcare", "TaraDescarcare").GroupBy(x => x.Text).Select(x => x.First()).ToList();
            ViewData["Rute"] = new SelectList(_context.Rute);
            return View();
        }

        // POST: Curse/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrasIncarcare,OrasDescarcare,Km,Pret,RutaID")] Cursa cursa)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cursa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RutaID"] = new SelectList(_context.Rute, "RutaID", "RutaID", cursa.RutaID);
            return View(cursa);
        }

        // GET: Curse/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cursa = await _context.Curse.FindAsync(id);
            if (cursa == null)
            {
                return NotFound();
            }
            ViewData["RutaID"] = new SelectList(_context.Rute, "RutaID", "RutaID", cursa.RutaID);
            return View(cursa);
        }

        // POST: Curse/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CursaID,OrasIncarcare,OrasDescarcare,Km,Pret,RutaID")] Cursa cursa)
        {
            if (id != cursa.CursaID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cursa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CursaExists(cursa.CursaID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RutaID"] = new SelectList(_context.Rute, "RutaID", "RutaID", cursa.RutaID);
            return View(cursa);
        }

        // GET: Curse/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cursa = await _context.Curse
                .Include(c => c.Ruta)
                .FirstOrDefaultAsync(m => m.CursaID == id);
            if (cursa == null)
            {
                return NotFound();
            }

            return View(cursa);
        }

        // POST: Curse/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cursa = await _context.Curse.FindAsync(id);
            _context.Curse.Remove(cursa);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CursaExists(int id)
        {
            return _context.Curse.Any(e => e.CursaID == id);
        }
    }
}
