﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LKW_Bursa_Transport.Areas.Identity.Data;
using LKW_Bursa_Transport.Models;
using Microsoft.AspNetCore.Identity;

namespace LKW_Bursa_Transport.Controllers
{
    public class ContabilitateController : Controller
    {
        private readonly AuthDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;

        public ContabilitateController(AuthDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        // GET: Contabilitate
        public async Task<IActionResult> Index()
        {
            var user = _userManager.GetUserAsync(User).Result;

            var cont = from c in _context.Contabilitate
                           .Include(v => v.Companie)
                           .Where(c => c.CompanieID == user.Id)
                           select c;

            return View(await cont.AsNoTracking().ToListAsync());
        }

        public async Task<IActionResult> AdaugaCursa(int? id)
        {

            Cursa cursaPreluata = await _context.Curse
                .Include(cursa => cursa.Ruta)
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.CursaID == id);

            if (cursaPreluata == null)
            {
                return NotFound();
            }

            var user = _userManager.GetUserAsync(User).Result;

            var contabilitate = await _context.Contabilitate
                .Include(c => c.Companie)
                .FirstOrDefaultAsync(m => m.CompanieID == user.Id);

            contabilitate.Venit += cursaPreluata.Pret;

            if (contabilitate.CurseEfectuate == null)
            {
                contabilitate.CurseEfectuate = new List<Cursa>();
            }

            contabilitate.CurseEfectuate.Add(cursaPreluata);
            
            await _context.SaveChangesAsync();

            return View(contabilitate);
        }

        // GET: Contabilitate/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contabilitate = await _context.Contabilitate
                .Include(c => c.Companie)
                .FirstOrDefaultAsync(m => m.ContabilitateID == id);
            if (contabilitate == null)
            {
                return NotFound();
            }

            return View(contabilitate);
        }

        // GET: Contabilitate/Create
        public IActionResult Create()
        {
            ViewData["CompanieID"] = new SelectList(_context.Users, "Id", "Id");
            return View();
        }

        // POST: Contabilitate/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ContabilitateID,Venit,Cheltuieli,CompanieID")] Contabilitate contabilitate)
        {
            if (ModelState.IsValid)
            {
                _context.Add(contabilitate);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanieID"] = new SelectList(_context.Users, "Id", "Id", contabilitate.CompanieID);
            return View(contabilitate);
        }

        // GET: Contabilitate/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contabilitate = await _context.Contabilitate.FindAsync(id);
            if (contabilitate == null)
            {
                return NotFound();
            }
            ViewData["CompanieID"] = new SelectList(_context.Users, "Id", "Id", contabilitate.CompanieID);
            return View(contabilitate);
        }

        // POST: Contabilitate/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ContabilitateID,Venit,Cheltuieli,CompanieID")] Contabilitate contabilitate)
        {
            if (id != contabilitate.ContabilitateID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contabilitate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContabilitateExists(contabilitate.ContabilitateID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanieID"] = new SelectList(_context.Users, "Id", "Id", contabilitate.CompanieID);
            return View(contabilitate);
        }

        // GET: Contabilitate/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contabilitate = await _context.Contabilitate
                .Include(c => c.Companie)
                .FirstOrDefaultAsync(m => m.ContabilitateID == id);
            if (contabilitate == null)
            {
                return NotFound();
            }

            return View(contabilitate);
        }

        // POST: Contabilitate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contabilitate = await _context.Contabilitate.FindAsync(id);
            _context.Contabilitate.Remove(contabilitate);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContabilitateExists(int id)
        {
            return _context.Contabilitate.Any(e => e.ContabilitateID == id);
        }
    }
}
