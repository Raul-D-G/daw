﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LKW_Bursa_Transport.Areas.Identity.Data;
using LKW_Bursa_Transport.Models;
using Microsoft.AspNetCore.Identity;

namespace LKW_Bursa_Transport.Controllers
{
    public class AngajatiController : Controller
    {
        private readonly AuthDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;

        public AngajatiController(AuthDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Angajati
        public async Task<IActionResult> Index(string sortOrder)
        {
            var user = _userManager.GetUserAsync(User).Result;
            var angajati = from angajat in _context.Angajati
                           .Include(angajat => angajat.Companie)
                           .Where(angajat => angajat.CompanieID == user.Id)
                           select angajat;

            ViewData["FunctieSortOrd"] = String.IsNullOrEmpty(sortOrder) ? "functie_desc" : "";
            ViewData["SalariuSortOrd"] = sortOrder == "Salariu" ? "salariu_desc" : "Salariu";

            switch (sortOrder)
            {
                case "functie_desc":
                    angajati = angajati.OrderByDescending(s => s.Functie);
                    break;
                case "Salariu":
                    angajati = angajati.OrderBy(s => s.Salariu);
                    break;
                case "salariu_desc":
                    angajati = angajati.OrderByDescending(s => s.Salariu);
                    break;
                default:
                    angajati = angajati.OrderBy(s => s.Functie);
                    break;
            }


            return View(await angajati.AsNoTracking().ToListAsync());
        }

        // GET: Angajati/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var angajat = await _context.Angajati
                .Include(a => a.Companie)
                .FirstOrDefaultAsync(m => m.AngajatID == id);
            if (angajat == null)
            {
                return NotFound();
            }

            return View(angajat);
        }

        // GET: Angajati/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Angajati/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Functie,Nume,Vechime,Salariu,Disponibil")] Angajat angajat)
        {
            try
            {
                var user = _userManager.GetUserAsync(User).Result;
                angajat.CompanieID = user.Id;
                if (ModelState.IsValid)
                {
                    _context.Add(angajat);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(angajat);
        }

        // GET: Angajati/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var angajat = await _context.Angajati.FindAsync(id);
            if (angajat == null)
            {
                return NotFound();
            }
            return View(angajat);
        }

        // POST: Angajati/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AngajatID,Functie,Nume,Vechime,Salariu,Disponibil")] Angajat angajat)
        {
            var user = _userManager.GetUserAsync(User).Result;
            angajat.CompanieID = user.Id;
            if (id != angajat.AngajatID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(angajat);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AngajatExists(angajat.AngajatID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(angajat);
        }

        // GET: Angajati/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var angajat = await _context.Angajati
                .Include(a => a.Companie)
                .FirstOrDefaultAsync(m => m.AngajatID == id);
            if (angajat == null)
            {
                return NotFound();
            }

            return View(angajat);
        }

        // POST: Angajati/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var angajat = await _context.Angajati.FindAsync(id);
            _context.Angajati.Remove(angajat);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AngajatExists(int id)
        {
            return _context.Angajati.Any(e => e.AngajatID == id);
        }
    }
}
