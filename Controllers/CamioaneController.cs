﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LKW_Bursa_Transport.Areas.Identity.Data;
using LKW_Bursa_Transport.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace LKW_Bursa_Transport.Controllers
{
    [Authorize]
    public class CamioaneController : Controller
    {
        private readonly AuthDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;

        public CamioaneController(AuthDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Camioane
        public async Task<IActionResult> Index(string sortOrder)
        {
            var user = _userManager.GetUserAsync(User).Result;

            var camioane = from c in _context.Camioane
                           .Include(v => v.Companie)
                           .Where(c => c.CompanieID == user.Id)
                           select c;

            ViewData["ConsumSortParam"] = String.IsNullOrEmpty(sortOrder) ? "camion_desc" : "";
            ViewData["NrSortOrd"] = sortOrder == "Nr" ? "nr_desc" : "Nr";

            switch (sortOrder)
            {
                case "camion_desc":
                    camioane = camioane.OrderByDescending(c => c.Consum);
                    break;
                case "Nr":
                    camioane = camioane.OrderBy(s => s.NrInmatriculare);
                    break;
                case "nr_desc":
                    camioane = camioane.OrderByDescending(s => s.NrInmatriculare);
                    break;
                default:
                    camioane = camioane.OrderBy(s => s.Consum);
                    break;
            }


            return View(await camioane.AsNoTracking().ToListAsync());
        }

        // GET: Camioane/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var camion = await _context.Camioane
                .Include(c => c.Companie)
                .FirstOrDefaultAsync(m => m.CamionID == id);
            if (camion == null)
            {
                return NotFound();
            }

            return View(camion);
        }

        // GET: Camioane/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Camioane/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CamionID,NrInmatriculare,Marca,Consum,Disponibil")] Camion camion)
        {
            try
            {
                /*      Adaug id-ul user(companie) logat in aplicatie       */
                var user = _userManager.GetUserAsync(User).Result;
                camion.CompanieID = user.Id;
                if (ModelState.IsValid)
                {
                    _context.Add(camion);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }
            return View(camion);
        }

        // GET: Camioane/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var camion = await _context.Camioane.FindAsync(id);
            if (camion == null)
            {
                return NotFound();
            }
            return View(camion);
        }

        // POST: Camioane/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CamionID,NrInmatriculare,Marca,Consum,Disponibil")] Camion camion)
        {
            var user = _userManager.GetUserAsync(User).Result;
            camion.CompanieID =  user.Id;
            if (id != camion.CamionID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(camion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CamionExists(camion.CamionID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(camion);
        }

        // GET: Camioane/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var camion = await _context.Camioane
                .Include(c => c.Companie)
                .FirstOrDefaultAsync(m => m.CamionID == id);
            if (camion == null)
            {
                return NotFound();
            }

            return View(camion);
        }

        // POST: Camioane/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var camion = await _context.Camioane.FindAsync(id);
            _context.Camioane.Remove(camion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CamionExists(int id)
        {
            return _context.Camioane.Any(e => e.CamionID == id);
        }
    }
}
