﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LKW_Bursa_Transport.Areas.Identity.Data;
using LKW_Bursa_Transport.Models;

namespace LKW_Bursa_Transport.Controllers
{
    public class RuteController : Controller
    {
        private readonly AuthDbContext _context;

        public RuteController(AuthDbContext context)
        {
            _context = context;
        }

        // GET: Rute
        public async Task<IActionResult> Index()
        {
            return View(await _context.Rute.ToListAsync());
        }

        // GET: Rute/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruta = await _context.Rute
                .Include(ruta => ruta.Curse)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.RutaID == id);
            if (ruta == null)
            {
                return NotFound();
            }

            return View(ruta);
        }

        // GET: Rute/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rute/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RutaID,TaraIncarcare,TaraDescarcare")] Ruta ruta)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ruta);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ruta);
        }

        // GET: Rute/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ruta = await _context.Rute
                .FirstOrDefaultAsync(m => m.RutaID == id);
            if (ruta == null)
            {
                return NotFound();
            }

            return View(ruta);
        }

        // POST: Rute/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ruta = await _context.Rute.FindAsync(id);
            _context.Rute.Remove(ruta);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RutaExists(int id)
        {
            return _context.Rute.Any(e => e.RutaID == id);
        }
    }
}
