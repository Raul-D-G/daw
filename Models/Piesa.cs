﻿using LKW_Bursa_Transport.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LKW_Bursa_Transport.Models
{
    public class Piesa
    {
        [Key]
        public int PiesaID { get; set; }

        [Column(TypeName = "nvarchar(100)"), Required, StringLength(100, MinimumLength = 3), Display(Name = "Denumire Piesa")]
        public string Denumire { get; set; }

        [DataType(DataType.Currency), Range(1, 10000), Required]
        public int Pret { get; set; }

        [Required, Range(1,100), Display(Name ="Numarul de piese disponibile")]
        public int NrPiesa { get; set; }

        public string CompanieID { get; set; }
        public virtual ApplicationUser Companie { get; set; }

    }
}
