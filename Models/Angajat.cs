﻿using LKW_Bursa_Transport.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LKW_Bursa_Transport.Models
{
    public class Angajat
    {
        [Key]
        public int AngajatID { get; set; }

        [Column(TypeName = "nvarchar(100)"), Required, StringLength(100, MinimumLength = 4)]
        public string Functie { get; set; }

        [Column(TypeName = "nvarchar(100)"), Required, StringLength(100, MinimumLength = 4), Display(Name = "Nume Angajat")]
        public string Nume { get; set; }

        [Required, Range(1, 50)]
        public int Vechime { get; set; }

        [DataType(DataType.Currency), Range(1, 10000)]
        public float Salariu { get; set; }

        [Required]
        public bool Disponibil { get; set; }

        public string CompanieID { get; set; }
        public virtual ApplicationUser Companie { get; set; }

    }
}
