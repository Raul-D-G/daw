﻿using LKW_Bursa_Transport.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LKW_Bursa_Transport.Models
{
    public class Camion
    {
        [Key]
        public int CamionID { get; set; }

        [Column(TypeName = "nvarchar(100)"), Required, StringLength(100, MinimumLength = 5), Display(Name = "Numar Inmatriculare")]
        public string NrInmatriculare { get; set; }

        [Column(TypeName = "nvarchar(100)"), Required, StringLength(100, MinimumLength = 2)]
        public string Marca { get; set; }

        [Required, Range(0, 100)]
        public int Consum { get; set; }

        [Required]
        public bool Disponibil { get; set; }

        public string CompanieID { get; set; }
        public virtual ApplicationUser Companie { get; set; }

    }
}
