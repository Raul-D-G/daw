﻿using LKW_Bursa_Transport.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LKW_Bursa_Transport.Models
{
    public class Contabilitate
    {
        [Key]
        public int ContabilitateID { get; set; }

        [Required]
        public int Venit { get; set; }

        [Required]
        public int Cheltuieli { get; set; }

        public ICollection<Cursa> CurseEfectuate { get; set; }


        public string CompanieID { get; set; }
        public virtual ApplicationUser Companie { get; set; }
    }
}
