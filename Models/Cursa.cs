﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LKW_Bursa_Transport.Models
{
    public class Cursa
    {
        [Key]
        public int CursaID { get; set; }

        [Required, Display(Name = "Oras Incarcare"), StringLength(100, MinimumLength = 4)]
        public string OrasIncarcare { get; set; }

        [Required, Display(Name = "Oras Descarcare"), StringLength(100, MinimumLength = 4)]
        public string OrasDescarcare { get; set; }

        public int Km { get; set; }
       
        [DataType(DataType.Currency), Range(1,10000), Required]
        public int Pret { get; set; }

        public int RutaID { get; set; }

        public Ruta Ruta { get; set; }
    }
}
