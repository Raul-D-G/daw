﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LKW_Bursa_Transport.Models
{
    public class Ruta
    {
        [Key]
        public int RutaID { get; set; }

        [Required, Display(Name = "Tara Incarcare"), StringLength(100, MinimumLength = 4)]
        public string TaraIncarcare { get; set; }

        [Required, Display(Name = "Tara Descarcare"), StringLength(100, MinimumLength = 4)]
        public string TaraDescarcare { get; set; }

        public virtual ICollection<Cursa> Curse { get; set; }
    }
}
