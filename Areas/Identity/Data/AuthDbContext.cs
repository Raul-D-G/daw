﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LKW_Bursa_Transport.Areas.Identity.Data;
using LKW_Bursa_Transport.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LKW_Bursa_Transport.Areas.Identity.Data
{
    public class AuthDbContext : IdentityDbContext<ApplicationUser>
    {
        public AuthDbContext(DbContextOptions<AuthDbContext> options)
            : base(options)
        {
        }

        public DbSet<Camion> Camioane { get; set; }
        public DbSet<Ruta> Rute { get; set; }
        public DbSet<Cursa> Curse { get; set; }
        public DbSet<Piesa> Piese { get; set; }
        public DbSet<Angajat> Angajati { get; set; }
        public DbSet<Contabilitate> Contabilitate { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Camion>()
               .HasOne(camion => camion.Companie)
               .WithMany(companie => companie.Camioane)
               .HasForeignKey(camion => camion.CompanieID)
               .OnDelete(DeleteBehavior.Cascade)
               .IsRequired();
            builder.Entity<Cursa>()
                .HasOne(cursa => cursa.Ruta)
                .WithMany(Ruta => Ruta.Curse)
                .HasForeignKey(cursa => cursa.RutaID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
            builder.Entity<Piesa>()
                .HasOne(piesa => piesa.Companie)
                .WithMany(companie => companie.Piese)
                .HasForeignKey(piesa => piesa.CompanieID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
            builder.Entity<Angajat>()
                .HasOne(angajat => angajat.Companie)
                .WithMany(companie => companie.Angajati)
                .HasForeignKey(angajat => angajat.CompanieID)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
            builder.Entity<Contabilitate>()
                .HasOne(contabilitate => contabilitate.Companie)
                .WithOne(companie => companie.Contabilitate)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();
        }
    }
}
