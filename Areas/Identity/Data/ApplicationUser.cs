﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using LKW_Bursa_Transport.Models;
using Microsoft.AspNetCore.Identity;

namespace LKW_Bursa_Transport.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [Column(TypeName ="nvarchar(100)"), Required, StringLength(100, MinimumLength = 3),
            Display(Name = "Nume Companie")]
        public string NumeCompanie { get; set; }

        [Column(TypeName = "nvarchar(100)"), Required, StringLength(100, MinimumLength = 5),
            Display(Name = "Adresa Companie")]
        public string AdresaCompanie { get; set; }

        [Required, Range(100000, 999999), Display(Name = "C.U.I. Companie")]
        public int CuiCompanie { get; set; }

        public virtual ICollection<Camion> Camioane { get; set; }

        public virtual ICollection<Piesa> Piese { get; set; }

        public virtual ICollection<Angajat> Angajati { get; set; }

        public virtual Contabilitate Contabilitate { get; set; }
    }
}
